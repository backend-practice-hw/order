CREATE TYPE "orders_type_enum" AS ENUM (
    'delivery',
    'pick_up'
);

CREATE TYPE "status_enum" AS ENUM (
    'accepted',
    'courier_accepted',
    'ready_in_branch',
    'on_way',
    'finished',
    'canceled'
);

CREATE TYPE "payment_type_enum" AS ENUM (
    'cash',
    'card'
);

CREATE TYPE "delivery_tariff_type_enum" AS ENUM (
    'fixed',
    'alternative'
);

CREATE TABLE "order_products" (
                                  "id" uuid PRIMARY KEY,
                                  "product_id" uuid,
                                  "quantity" int,
                                  "price" float,
                                  "order_id" uuid,
                                  "created_at" timestamp default 'Now()',
                                  "updated_at" timestamp default 'Now()',
                                  "deleted_at" integer default 0
);

CREATE TABLE "orders" (
                          "id" uuid PRIMARY KEY,
                          "order_id" serial not null ,
                          "client_id" uuid,
                          "branch_id" uuid,
                          "type" orders_type_enum,
                          "address" text,
                          "courier_id" uuid,
                          "price" float,
                          "delivery_price" float,
                          "discount" float,
                          "status" status_enum,
                          "payment_type" payment_type_enum,
                          "created_at" timestamp default 'Now()',
                          "updated_at" timestamp default 'Now()',
                          "deleted_at" integer default 0
);

CREATE TABLE "alternative_values" (
                                      "id" text PRIMARY KEY,
                                      "from_price" float,
                                      "to_price" float,
                                      "price" float,
                                      "created_at" timestamp default 'Now()',
                                      "updated_at" timestamp default 'Now()',
                                      "deleted_at" integer default 0
);

CREATE TABLE "delivery_tariffs" (
                                    "id" uuid PRIMARY KEY,
                                    "name" varchar(100),
                                    "type" delivery_tariff_type_enum,
                                    "base_price" float,
                                    "alternative_value" text,
                                    "created_at" timestamp default 'Now()',
                                    "updated_at" timestamp default 'Now()',
                                    "deleted_at" integer default 0
);



ALTER TABLE "order_products" ADD FOREIGN KEY ("order_id") REFERENCES "orders" ("id");

ALTER TABLE "delivery_tariffs" ADD FOREIGN KEY ("alternative_value") REFERENCES "alternative_values" ("id");
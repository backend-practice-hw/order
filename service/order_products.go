package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbC "order_service/genproto/catalog_service"
	pbO "order_service/genproto/order_service"
	"order_service/grpc/client"
	"order_service/pkg/logger"
	"order_service/storage"
)

type al struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewOrderProductServiceService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *al {
	return &al{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (o *al) Create(ctx context.Context, request *pbO.OrderProduct) (*pbO.OrderProduct, error) {
	product, err := o.services.ProductService().Get(ctx, &pbC.PrimaryKey{Id: request.ProductId})
	if err != nil {
		o.log.Error("error is while getting product", logger.Error(err))
		return nil, err
	}

	price := float32(request.Quantity) + product.Price
	request.Price = price

	return o.storage.OrderProducts().Create(ctx, request)
}

func (o *al) Get(ctx context.Context, key *pbO.PrimaryKey) (*pbO.OrderProduct, error) {
	return o.storage.OrderProducts().Get(ctx, key)
}

func (o *al) GetList(ctx context.Context, request *pbO.OrderProductRequest) (*pbO.OrderProductResponse, error) {
	return o.storage.OrderProducts().GetList(ctx, request)
}

func (o *al) Update(ctx context.Context, request *pbO.OrderProduct) (*pbO.OrderProduct, error) {
	return o.storage.OrderProducts().Update(ctx, request)
}

func (o *al) Delete(ctx context.Context, key *pbO.PrimaryKey) (*empty.Empty, error) {
	return o.storage.OrderProducts().Delete(ctx, key)
}

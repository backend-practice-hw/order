package service

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	pbO "order_service/genproto/order_service"
	pbU "order_service/genproto/user_service"
	"order_service/grpc/client"
	"order_service/pkg/generate_id"
	"order_service/pkg/logger"
	"order_service/storage"
	"time"
)

type orderService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewOrderService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *orderService {
	return &orderService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (o *orderService) Create(ctx context.Context, request *pbO.Order) (*pbO.Order, error) {
	emptyMsg := pbO.Empty{}

	id, err := o.storage.Order().GetLastBranchOrderID(ctx, &emptyMsg)
	if err != nil {
		o.log.Error("error in service layer is while getting last branch order id", logger.Error(err))
		return nil, err
	}

	branchData, err := o.services.BranchService().Get(ctx, &pbU.PrimaryKey{Id: request.BranchId})
	if err != nil {
		o.log.Error("error in service layer while getting branch data", logger.Error(err))
		return nil, err
	}

	orderID := generate_id.GenerateBranchID(branchData.Name, id.OrderId)
	fmt.Println("branch name", branchData.Name)
	request.OrderId = orderID

	order, err := o.storage.Order().Create(ctx, request)
	if err != nil {
		o.log.Error("error in service layer while creating order", logger.Error(err))
		return nil, err
	}

	return order, nil
}

func (o *orderService) Get(ctx context.Context, key *pbO.PrimaryKey) (*pbO.Order, error) {
	return o.storage.Order().Get(ctx, key)
}

func (o *orderService) GetList(ctx context.Context, request *pbO.OrderRequest) (*pbO.OrderResponse, error) {
	return o.storage.Order().GetList(ctx, request)
}

func (o *orderService) Update(ctx context.Context, request *pbO.Order) (*pbO.Order, error) {
	return o.storage.Order().Update(ctx, request)
}

func (o *orderService) Delete(ctx context.Context, key *pbO.PrimaryKey) (*empty.Empty, error) {
	return o.storage.Order().Delete(ctx, key)
}

func (o *orderService) GetLastBranchOrderID(ctx context.Context, em *pbO.Empty) (*pbO.OrderID, error) {
	return o.storage.Order().GetLastBranchOrderID(ctx, em)

}

func (o *orderService) StartOrder(ctx context.Context, order *pbO.Order) (*pbO.Order, error) {
	return o.storage.Order().Create(ctx, order)
}

func (o *orderService) EndOrder(ctx context.Context, order *pbO.EndOrderRequest) (*pbO.Order, error) {
	ordersResp, err := o.storage.OrderProducts().GetList(ctx, &pbO.OrderProductRequest{
		Page:    1,
		Limit:   10,
		OrderId: order.Id,
	})
	if err != nil {
		o.log.Error("error is while getting order products list", logger.Error(err))
		return nil, err
	}

	orders := make(map[string]*pbO.OrderProduct)
	totalPrice := float32(0.0)

	for _, product := range ordersResp.GetOrderProducts() {
		orders[product.OrderId] = product
		totalPrice += product.Price
	}

	clientData, err := o.services.ClientService().Get(ctx, &pbU.PrimaryKey{Id: order.ClientId})
	if err != nil {
		o.log.Error("error is while getting client data", logger.Error(err))
		return nil, err
	}
	// discount hisoblash
	order.Discount = totalPrice - clientData.DiscountAmount

	// client data update
	o.services.ClientService().Update(ctx, &pbU.Client{
		Id:               order.ClientId,
		LastOrderedDate:  time.Now().String(),
		TotalOrdersSum:   totalPrice,
		TotalOrdersCount: ordersResp.Count,
	})

	branchData, err := o.services.BranchService().Get(ctx, &pbU.PrimaryKey{Id: order.BranchId})
	if err != nil {
		o.log.Error("error is while getting branch data", logger.Error(err))
		return nil, err
	}

	deliveryTariffData, err := o.services.DeliveryTariffService().Get(ctx, &pbO.PrimaryKey{Id: branchData.DeliveryTariffId})
	if err != nil {
		o.log.Error("error is while getting delivery tariff data", logger.Error(err))
		return nil, err
	}

	deliveryPrice := float32(0.0)

	// delivery_price logic
	if deliveryTariffData.Type == "fixed" {
		deliveryPrice += deliveryTariffData.BasePrice
	} else if deliveryTariffData.Type == "alternative" {
		alternativeData, err := o.services.AlternativeValueService().GetList(ctx, &pbO.GetAlternativeValueRequest{
			Page:  1,
			Limit: 10,
		})
		if err != nil {
			o.log.Error("error is while getting alternative value", logger.Error(err))
			return nil, err
		}

		for _, alternativeValue := range alternativeData.AlternativeValues {
			if alternativeValue.FromPrice < totalPrice && alternativeValue.ToPrice > totalPrice {
				deliveryPrice += alternativeValue.Price
			}
		}

	}

	// updating end order
	endOrderData, err := o.storage.Order().Update(ctx, &pbO.Order{
		Id:            order.Id,
		Price:         totalPrice,
		DeliveryPrice: deliveryPrice,
	})
	if err != nil {
		o.log.Error("error is while updating end order", logger.Error(err))
		return nil, err
	}

	return endOrderData, nil
}

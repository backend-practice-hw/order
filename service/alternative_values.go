package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbO "order_service/genproto/order_service"
	"order_service/grpc/client"
	"order_service/pkg/logger"
	"order_service/storage"
)

type alternativeValuesService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewAlternativeValuesService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *alternativeValuesService {
	return &alternativeValuesService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (a *alternativeValuesService) Create(ctx context.Context, request *pbO.AlternativeValue) (*pbO.AlternativeValue, error) {
	return a.storage.AlternativeValue().Create(ctx, request)
}

func (a *alternativeValuesService) Get(ctx context.Context, key *pbO.PrimaryKey) (*pbO.AlternativeValue, error) {
	return a.storage.AlternativeValue().Get(ctx, key)
}

func (a *alternativeValuesService) GetList(ctx context.Context, request *pbO.GetAlternativeValueRequest) (*pbO.GetAlternativeValueResponse, error) {
	return a.storage.AlternativeValue().GetList(ctx, request)
}

func (a *alternativeValuesService) Update(ctx context.Context, request *pbO.AlternativeValue) (*pbO.AlternativeValue, error) {
	return a.storage.AlternativeValue().Update(ctx, request)
}

func (a *alternativeValuesService) Delete(ctx context.Context, key *pbO.PrimaryKey) (*empty.Empty, error) {
	return a.storage.AlternativeValue().Delete(ctx, key)
}

package service

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbO "order_service/genproto/order_service"
	"order_service/grpc/client"
	"order_service/pkg/logger"
	"order_service/storage"
)

type deliveryTariffService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewDeliveryTariffService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *deliveryTariffService {
	return &deliveryTariffService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (d *deliveryTariffService) Create(ctx context.Context, request *pbO.DeliveryTariff) (*pbO.DeliveryTariff, error) {
	return d.storage.DeliveryTariff().Create(ctx, request)
}

func (d *deliveryTariffService) Get(ctx context.Context, key *pbO.PrimaryKey) (*pbO.DeliveryTariff, error) {
	return d.storage.DeliveryTariff().Get(ctx, key)
}

func (d *deliveryTariffService) GetList(ctx context.Context, request *pbO.DeliveryTariffRequest) (*pbO.DeliveryTariffResponse, error) {
	return d.storage.DeliveryTariff().GetList(ctx, request)
}

func (d *deliveryTariffService) Update(ctx context.Context, request *pbO.DeliveryTariff) (*pbO.DeliveryTariff, error) {
	return d.storage.DeliveryTariff().Update(ctx, request)
}

func (d *deliveryTariffService) Delete(ctx context.Context, key *pbO.PrimaryKey) (*empty.Empty, error) {
	return d.storage.DeliveryTariff().Delete(ctx, key)
}

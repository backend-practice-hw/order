package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbO "order_service/genproto/order_service"
	"order_service/pkg/helper"
	"order_service/pkg/logger"
	"order_service/storage"
)

type orderRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewOrderRepo(db *pgxpool.Pool, log logger.ILogger) storage.IOrderStorage {
	return &orderRepo{db: db, log: log}
}

func (o *orderRepo) Create(ctx context.Context, request *pbO.Order) (*pbO.Order, error) {
	id := uuid.New()
	resp := pbO.Order{}

	query := `insert into orders (id, order_id, client_id, branch_id, type, address, courier_id, price, 
                      delivery_price, discount, status, payment_type) 
		values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
		returning id, order_id, client_id, branch_id, type, address, courier_id, price, 
                      delivery_price, discount, status, payment_type, created_at::text, updated_at::text`

	if err := o.db.QueryRow(ctx, query,
		id,
		request.GetOrderId(),
		request.GetClientId(),
		request.GetBranchId(),
		request.GetType(),
		request.GetAddress(),
		request.GetCourierId(),
		request.GetPrice(),
		request.GetDeliveryPrice(),
		request.GetDiscount(),
		request.GetStatus(),
		request.GetPaymentType(),
	).Scan(
		&resp.Id,
		&resp.OrderId,
		&resp.ClientId,
		&resp.BranchId,
		&resp.Type,
		&resp.Address,
		&resp.CourierId,
		&resp.Price,
		&resp.DeliveryPrice,
		&resp.Discount,
		&resp.Status,
		&resp.PaymentType,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		fmt.Println("error is while inserting data to order", err.Error())
		return &pbO.Order{}, err
	}

	return &resp, nil
}

func (o *orderRepo) Get(ctx context.Context, key *pbO.PrimaryKey) (*pbO.Order, error) {
	resp := pbO.Order{}
	query := `select id, order_id, client_id, branch_id, type, address, courier_id, price, 
                    delivery_price, discount, status, payment_type, created_at::text, updated_at::text
                                   from orders where id = $1 and deleted_at = 0`
	if err := o.db.QueryRow(ctx, query, key.GetId()).Scan(
		&resp.Id,
		&resp.OrderId,
		&resp.ClientId,
		&resp.BranchId,
		&resp.Type,
		&resp.Address,
		&resp.CourierId,
		&resp.Price,
		&resp.DeliveryPrice,
		&resp.Discount,
		&resp.Status,
		&resp.PaymentType,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		fmt.Println("error is while selecting data from orders", err.Error())
		return &pbO.Order{}, err
	}

	return &resp, nil
}

func (o *orderRepo) GetList(ctx context.Context, request *pbO.OrderRequest) (*pbO.OrderResponse, error) {
	var (
		count                     = 0
		orders                    []*pbO.Order
		query, filter, countQuery string
		offset                    = (request.GetPage() - 1) * request.GetLimit()
		pagination                string
	)

	pagination += ` ORDER BY created_at desc LIMIT $1 OFFSET $2`

	if request.GetOrderId() != "" {
		filter += fmt.Sprintf(` and order_id =  '%s'`, request.GetOrderId())
	}
	if request.GetClientId() != "" {
		filter += fmt.Sprintf(` and client_id =  '%s'`, request.GetClientId())
	}
	if request.GetBranchId() != "" {
		filter += fmt.Sprintf(` and branch_id =  '%s'`, request.GetBranchId())
	}
	if request.GetCourierId() != "" {
		filter += fmt.Sprintf(` and courier_id =  '%s'`, request.GetCourierId())
	}
	if request.GetFromPrice() != 0.0 && request.GetToPrice() != 0.0 {
		filter += fmt.Sprintf(` and price between %.1f and %.1f`, request.GetFromPrice(), request.GetToPrice())
	}
	if request.GetFromPrice() != 0.0 {
		filter += fmt.Sprintf(` and price <= %.1f`, request.GetFromPrice())
	}
	if request.GetToPrice() != 0.0 {
		filter += fmt.Sprintf(` and price >= %.1f`, request.GetToPrice())
	}
	if request.GetType() != "" {
		filter += fmt.Sprintf(` and type = '%s'`, request.GetType())
	}
	if request.GetStatusType() != "" {
		filter += fmt.Sprintf(` and status = '%s'`, request.GetStatusType())
	}
	if request.GetPaymentType() != "" {
		filter += fmt.Sprintf(` and payment_type = '%s'`, request.GetPaymentType())
	}

	countQuery = `select count(1) from orders where deleted_at = 0 ` + filter

	if err := o.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		fmt.Println("error is while scanning count", err.Error())
		return &pbO.OrderResponse{}, err
	}

	query = `select id, order_id, client_id, branch_id, type, address, courier_id, price, 
                    delivery_price, discount, status, payment_type, created_at::text, updated_at::text
                            from orders where deleted_at = 0  ` + filter + pagination
	rows, err := o.db.Query(ctx, query, request.GetLimit(), offset)
	if err != nil {
		fmt.Println("error is while selecting * from orders", err.Error())
		return &pbO.OrderResponse{}, err
	}

	for rows.Next() {
		order := pbO.Order{}
		if err := rows.Scan(
			&order.Id,
			&order.OrderId,
			&order.ClientId,
			&order.BranchId,
			&order.Type,
			&order.Address,
			&order.CourierId,
			&order.Price,
			&order.DeliveryPrice,
			&order.Discount,
			&order.Status,
			&order.PaymentType,
			&order.CreatedAt,
			&order.UpdatedAt,
		); err != nil {
			fmt.Println("error is while scanning orders", err.Error())
			return &pbO.OrderResponse{}, err
		}
		orders = append(orders, &order)
	}

	return &pbO.OrderResponse{
		Orders: orders,
		Count:  int32(count),
	}, nil
}

func (o *orderRepo) Update(ctx context.Context, request *pbO.Order) (*pbO.Order, error) {
	var (
		filter = ``
		params = make(map[string]interface{})
		query  = `update orders set `
		resp   = pbO.Order{}
	)

	params["id"] = request.GetId()
	fmt.Println("param", params)

	if request.GetClientId() != "" {
		params[" client_id"] = request.GetClientId()

		filter += " client_id = @client_id,"
	}

	if request.GetBranchId() != "" {
		params[" branch_id"] = request.GetBranchId()

		filter += " branch_id = @branch_id,"
	}

	if request.GetType() != "" {
		params[" type"] = request.GetType()

		filter += " type = @type,"
	}

	if request.GetAddress() != "" {
		params[" address"] = request.GetAddress()

		filter += " address = @address,"
	}

	if request.GetCourierId() != "" {
		params[" courier_id"] = request.GetCourierId()

		filter += " courier_id = @courier_id,"
	}

	if request.GetPrice() != 0.0 {
		params[" price"] = request.GetPrice()

		filter += " price = @price,"
	}

	if request.GetDeliveryPrice() != 0.0 {
		params[" delivery_price"] = request.GetDeliveryPrice()

		filter += " delivery_price = @delivery_price,"
	}

	if request.GetDiscount() != 0.0 {
		params[" discount"] = request.GetDiscount()

		filter += " discount = @discount,"
	}

	if request.GetStatus() != "" {
		params[" status"] = request.GetStatus()

		filter += " status = @status,"
	}

	if request.GetPaymentType() != "" {
		params[" payment_type"] = request.GetPaymentType()

		filter += " payment_type = @payment_type,"
	}

	query += filter + ` updated_at = Now() where id = @id
        returning id, price,  delivery_price`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	fmt.Println("arg", args)
	fmt.Println("fullQuery", fullQuery)
	if err := o.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
		&resp.Price,
		&resp.DeliveryPrice,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (o *orderRepo) Delete(ctx context.Context, key *pbO.PrimaryKey) (*empty.Empty, error) {
	query := `update orders set deleted_at = extract(epoch from current_timestamp) where id = $1`

	_, err := o.db.Exec(ctx, query, key.GetId())
	if err != nil {
		fmt.Println("error is while deleting order", err.Error())
		return nil, err
	}
	return nil, nil
}

func (o *orderRepo) GetLastBranchOrderID(ctx context.Context, empty2 *pbO.Empty) (*pbO.OrderID, error) {
	var orderID string
	query := `select order_id from orders order by order_id desc limit 1`
	if err := o.db.QueryRow(ctx, query).Scan(&orderID); err != nil {
		o.log.Error("error is while getting order id", logger.Error(err))
		return &pbO.OrderID{}, err
	}
	return &pbO.OrderID{OrderId: orderID}, nil
}

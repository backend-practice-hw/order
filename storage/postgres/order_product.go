package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbO "order_service/genproto/order_service"
	"order_service/pkg/logger"
	"order_service/storage"
)

type orderProductRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewOrderProductRepo(db *pgxpool.Pool, log logger.ILogger) storage.IOrderProductStorage {
	return &orderProductRepo{db: db, log: log}
}

func (o *orderProductRepo) Create(ctx context.Context, request *pbO.OrderProduct) (*pbO.OrderProduct, error) {
	id := uuid.New()
	resp := pbO.OrderProduct{}

	query := `insert into order_products (id, product_id, order_id ,quantity, price) 
									values($1, $2, $3, $4, $5)
		returning id, product_id, quantity, price, order_id, created_at::text, updated_at::text`

	if err := o.db.QueryRow(ctx, query,
		id,
		request.GetProductId(),
		request.GetOrderId(),
		request.GetQuantity(),
		request.GetPrice(),
	).Scan(
		&resp.Id,
		&resp.ProductId,
		&resp.Quantity,
		&resp.Price,
		&resp.OrderId,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		fmt.Println("error is while inserting data", err.Error())
		return &pbO.OrderProduct{}, err
	}

	return &resp, nil
}

func (o *orderProductRepo) Get(ctx context.Context, key *pbO.PrimaryKey) (*pbO.OrderProduct, error) {
	resp := pbO.OrderProduct{}
	query := `select id, product_id, quantity, price, order_id, created_at::text, updated_at::text
                                   from order_products where id = $1 and deleted_at = 0`
	if err := o.db.QueryRow(ctx, query, key.GetId()).Scan(
		&resp.Id,
		&resp.ProductId,
		&resp.Quantity,
		&resp.Price,
		&resp.OrderId,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		fmt.Println("error is while selecting data from order product", err.Error())
		return &pbO.OrderProduct{}, err
	}

	return &resp, nil
}

func (o *orderProductRepo) GetList(ctx context.Context, request *pbO.OrderProductRequest) (*pbO.OrderProductResponse, error) {
	var (
		count                     = 0
		orderProducts             []*pbO.OrderProduct
		query, filter, countQuery string
		offset                    = (request.GetPage() - 1) * request.GetLimit()
		pagination                string
	)

	pagination += ` ORDER BY created_at desc LIMIT $1 OFFSET $2`

	if request.GetProductId() != "" {
		filter += fmt.Sprintf(` and product_id =  '%s'`, request.GetProductId())
	}
	if request.GetFromPrice() != 0.0 && request.GetToPrice() != 0.0 {
		filter += fmt.Sprintf(` and price between %.1f and %.1f`, request.GetFromPrice(), request.GetToPrice())
	}
	if request.GetFromPrice() != 0.0 {
		filter += fmt.Sprintf(` and price <= %.1f`, request.GetFromPrice())
	}
	if request.GetToPrice() != 0.0 {
		filter += fmt.Sprintf(` and price >= %.1f`, request.GetToPrice())
	}
	if request.GetOrderId() != "" {
		filter += fmt.Sprintf(` and order_id = '%s'`, request.GetOrderId())
	}

	countQuery = `select count(1) from order_products where deleted_at = 0 ` + filter

	if err := o.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		fmt.Println("error is while scanning count", err.Error())
		return &pbO.OrderProductResponse{}, err
	}

	query = `select id, product_id, quantity, price, order_id, created_at::text, updated_at::text 
                            from order_products where deleted_at = 0  ` + filter + pagination
	rows, err := o.db.Query(ctx, query, request.GetLimit(), offset)
	if err != nil {
		fmt.Println("error is while selecting * from order products", err.Error())
		return &pbO.OrderProductResponse{}, err
	}

	for rows.Next() {
		orderProduct := pbO.OrderProduct{}
		if err := rows.Scan(
			&orderProduct.Id,
			&orderProduct.ProductId,
			&orderProduct.Quantity,
			&orderProduct.Price,
			&orderProduct.OrderId,
			&orderProduct.CreatedAt,
			&orderProduct.UpdatedAt,
		); err != nil {
			fmt.Println("error is while scanning order products", err.Error())
			return &pbO.OrderProductResponse{}, err
		}
		orderProducts = append(orderProducts, &orderProduct)
	}

	return &pbO.OrderProductResponse{
		OrderProducts: orderProducts,
		Count:         int32(count),
	}, nil
}

func (o *orderProductRepo) Update(ctx context.Context, request *pbO.OrderProduct) (*pbO.OrderProduct, error) {
	resp := pbO.OrderProduct{}
	query := `update order_products set product_id = $1, quantity = $2, price = $3, order_id = $4,
                    updated_at = Now() where id = $5 
                    returning id, product_id, quantity, price, order_id, created_at::text, updated_at::text`

	if err := o.db.QueryRow(ctx, query,
		request.GetProductId(),
		request.GetQuantity(),
		request.GetPrice(),
		request.GetOrderId(),
		request.GetId(),
	).Scan(
		&resp.Id,
		&resp.ProductId,
		&resp.Quantity,
		&resp.Price,
		&resp.OrderId,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		fmt.Println("error is while updating order products", err.Error())
		return &pbO.OrderProduct{}, err
	}

	return &resp, nil
}

func (o *orderProductRepo) Delete(ctx context.Context, key *pbO.PrimaryKey) (*empty.Empty, error) {
	query := `update order_products set deleted_at = extract(epoch from current_timestamp) where id = $1`

	_, err := o.db.Exec(ctx, query, key.GetId())
	if err != nil {
		fmt.Println("error is while deleting order product", err.Error())
		return nil, err
	}
	return nil, nil
}

package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbO "order_service/genproto/order_service"
	"order_service/pkg/logger"
)

type alternativeValueRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewAlternativeValueRepo(db *pgxpool.Pool, log logger.ILogger) *alternativeValueRepo {
	return &alternativeValueRepo{
		db:  db,
		log: log,
	}
}

func (a *alternativeValueRepo) Create(ctx context.Context, request *pbO.AlternativeValue) (*pbO.AlternativeValue, error) {
	alternativeValue := pbO.AlternativeValue{}
	query := `insert into alternative_values(id, from_price, to_price, price) values($1, $2, $3, $4)
    returning id, from_price, to_price, price, created_at::text, updated_at::text`
	if err := a.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.GetFromPrice(),
		request.GetToPrice(),
		request.GetPrice(),
	).Scan(
		&alternativeValue.Id,
		&alternativeValue.FromPrice,
		&alternativeValue.ToPrice,
		&alternativeValue.Price,
		&alternativeValue.CreatedAt,
		&alternativeValue.UpdatedAt,
	); err != nil {
		a.log.Error("error is while inserting into alternative value", logger.Error(err))
		return nil, err
	}
	return &alternativeValue, nil
}

func (a *alternativeValueRepo) Get(ctx context.Context, key *pbO.PrimaryKey) (*pbO.AlternativeValue, error) {
	alternativeValue := pbO.AlternativeValue{}
	query := `select id, from_price, to_price, price, created_at::text, updated_at::text from alternative_values where id = $1 and deleted_at = 0`
	if err := a.db.QueryRow(ctx, query, key.GetId()).Scan(
		&alternativeValue.Id,
		&alternativeValue.FromPrice,
		&alternativeValue.ToPrice,
		&alternativeValue.Price,
		&alternativeValue.CreatedAt,
		&alternativeValue.UpdatedAt,
	); err != nil {
		a.log.Error("error is while selecting alternative value by id", logger.Error(err))
		return nil, err
	}
	return &alternativeValue, nil
}

func (a *alternativeValueRepo) GetList(ctx context.Context, request *pbO.GetAlternativeValueRequest) (*pbO.GetAlternativeValueResponse, error) {
	var (
		count                     = 0
		alternativeValues         []*pbO.AlternativeValue
		query, filter, countQuery string
		offset                    = (request.GetPage() - 1) * request.GetLimit()
		pagination                string
	)

	pagination += ` ORDER BY created_at desc LIMIT $1 OFFSET $2`

	if request.GetFromPrice() != 0.0 {
		filter += fmt.Sprintf(` and from_price >= %.1f`, request.GetFromPrice())
	}
	if request.GetToPrice() != 0.0 {
		filter += fmt.Sprintf(` and to_price <= %.1f`, request.GetToPrice())
	}

	countQuery = `select count(1) from alternative_values where deleted_at = 0 ` + filter

	if err := a.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		fmt.Println("error is while scanning count", err.Error())
		return &pbO.GetAlternativeValueResponse{}, err
	}

	query = `select id, from_price, to_price, price, created_at::text, updated_at::text from alternative_values where deleted_at = 0  ` + filter + pagination
	rows, err := a.db.Query(ctx, query, request.GetLimit(), offset)
	if err != nil {
		fmt.Println("error is while selecting * from alternative values", err.Error())
		return &pbO.GetAlternativeValueResponse{}, err
	}

	for rows.Next() {
		alternativeValue := pbO.AlternativeValue{}
		if err := rows.Scan(
			&alternativeValue.Id,
			&alternativeValue.FromPrice,
			&alternativeValue.ToPrice,
			&alternativeValue.Price,
			&alternativeValue.CreatedAt,
			&alternativeValue.UpdatedAt,
		); err != nil {
			fmt.Println("error is while scanning alternative values", err.Error())
			return &pbO.GetAlternativeValueResponse{}, err
		}
		alternativeValues = append(alternativeValues, &alternativeValue)
	}

	return &pbO.GetAlternativeValueResponse{
		AlternativeValues: alternativeValues,
		Count:             int32(count),
	}, nil
}

func (a *alternativeValueRepo) Update(ctx context.Context, request *pbO.AlternativeValue) (*pbO.AlternativeValue, error) {
	alternativeValue := pbO.AlternativeValue{}
	query := `update alternative_values set from_price = $1, to_price = $2, price = $3, updated_at = now() where id = $4 and deleted_at = 0 
                   returning id, from_price, to_price, price, created_at::text, updated_at::text`
	if err := a.db.QueryRow(ctx, query,
		request.GetFromPrice(),
		request.GetToPrice(),
		request.GetPrice(),
		request.GetId(),
	).Scan(
		&alternativeValue.Id,
		&alternativeValue.FromPrice,
		&alternativeValue.ToPrice,
		&alternativeValue.Price,
		&alternativeValue.CreatedAt,
		&alternativeValue.UpdatedAt,
	); err != nil {
		a.log.Error("error is while updating alternative value", logger.Error(err))
		return nil, err
	}
	return &alternativeValue, nil
}

func (a *alternativeValueRepo) Delete(ctx context.Context, key *pbO.PrimaryKey) (*empty.Empty, error) {
	query := `update alternative_values set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := a.db.Exec(ctx, query, key.GetId())
	if err != nil {
		fmt.Println("error is while deleting alternative value", logger.Error(err))
		return nil, err
	}
	return nil, nil
}

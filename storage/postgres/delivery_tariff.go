package postgres

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	pbO "order_service/genproto/order_service"
	"order_service/pkg/logger"
)

type deliveryTariffRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewDeliveryTariffRepo(db *pgxpool.Pool, log logger.ILogger) *deliveryTariffRepo {
	return &deliveryTariffRepo{
		db:  db,
		log: log,
	}
}

func (d *deliveryTariffRepo) Create(ctx context.Context, request *pbO.DeliveryTariff) (*pbO.DeliveryTariff, error) {
	resp := pbO.DeliveryTariff{}
	query := `insert into delivery_tariffs(id, name, type, base_price, alternative_value)
                         values($1, $2, $3, $4, $5)
    returning id, name, type, base_price, alternative_value, created_at::text, updated_at::text`
	if err := d.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.GetName(),
		request.GetType(),
		request.GetBasePrice(),
		request.GetAlternativeValue(),
	).Scan(
		&resp.Id,
		&resp.Name,
		&resp.Type,
		&resp.BasePrice,
		&resp.AlternativeValue,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		d.log.Error("error is while inserting into delivery tariff", logger.Error(err))
		return nil, err
	}
	return &resp, nil
}

func (d *deliveryTariffRepo) Get(ctx context.Context, key *pbO.PrimaryKey) (*pbO.DeliveryTariff, error) {
	deliveryTariff := pbO.DeliveryTariff{}
	query := `select id, name, type, base_price, alternative_value, created_at::text, updated_at::text
                         from delivery_tariffs where id = $1 and deleted_at = 0`
	if err := d.db.QueryRow(ctx, query, key.GetId()).Scan(
		&deliveryTariff.Id,
		&deliveryTariff.Name,
		&deliveryTariff.Type,
		&deliveryTariff.BasePrice,
		&deliveryTariff.AlternativeValue,
		&deliveryTariff.CreatedAt,
		&deliveryTariff.UpdatedAt,
	); err != nil {
		d.log.Error("error is while selecting delivery tariff by id", logger.Error(err))
		return nil, err
	}
	return &deliveryTariff, nil
}

func (d *deliveryTariffRepo) GetList(ctx context.Context, request *pbO.DeliveryTariffRequest) (*pbO.DeliveryTariffResponse, error) {
	var (
		count                     = 0
		deliveryTariffs           []*pbO.DeliveryTariff
		query, filter, countQuery string
		offset                    = (request.GetPage() - 1) * request.GetLimit()
		pagination                string
	)

	pagination += ` ORDER BY created_at desc LIMIT $1 OFFSET $2`

	if request.GetName() != "" {
		filter += fmt.Sprintf(` and name ilike '%s'`, request.GetName())
	}
	if request.GetType() != "" {
		filter += fmt.Sprintf(` and type =  '%s'`, request.GetType())
	}

	countQuery = `select count(1) from delivery_tariffs where deleted_at = 0 ` + filter

	if err := d.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		fmt.Println("error is while scanning count", err.Error())
		return &pbO.DeliveryTariffResponse{}, err
	}

	query = `select id, name, type, base_price, alternative_value, created_at::text, updated_at::text
                         from delivery_tariffs where deleted_at = 0  ` + filter + pagination
	rows, err := d.db.Query(ctx, query, request.GetLimit(), offset)
	if err != nil {
		fmt.Println("error is while selecting * from delivery tariffs", err.Error())
		return &pbO.DeliveryTariffResponse{}, err
	}

	for rows.Next() {
		deliveryTariff := pbO.DeliveryTariff{}
		if err := rows.Scan(
			&deliveryTariff.Id,
			&deliveryTariff.Name,
			&deliveryTariff.Type,
			&deliveryTariff.BasePrice,
			&deliveryTariff.AlternativeValue,
			&deliveryTariff.CreatedAt,
			&deliveryTariff.UpdatedAt,
		); err != nil {
			fmt.Println("error is while scanning delivery tariff", err.Error())
			return &pbO.DeliveryTariffResponse{}, err
		}
		deliveryTariffs = append(deliveryTariffs, &deliveryTariff)
	}

	return &pbO.DeliveryTariffResponse{
		DeliveryTariffs: deliveryTariffs,
		Count:           int32(count),
	}, nil
}

func (d *deliveryTariffRepo) Update(ctx context.Context, request *pbO.DeliveryTariff) (*pbO.DeliveryTariff, error) {
	resp := pbO.DeliveryTariff{}
	query := `update delivery_tariffs set name = $1, type = $2, base_price = $3, alternative_value = $4,
                   updated_at = now()  where id = $5 and deleted_at = 0 
                   returning id, name, type, base_price, alternative_value, created_at::text, updated_at::text`
	if err := d.db.QueryRow(ctx, query,
		request.GetName(),
		request.GetType(),
		request.GetBasePrice(),
		request.GetAlternativeValue(),
		request.GetId(),
	).Scan(
		&resp.Id,
		&resp.Name,
		&resp.Type,
		&resp.BasePrice,
		&resp.AlternativeValue,
		&resp.CreatedAt,
		&resp.UpdatedAt,
	); err != nil {
		d.log.Error("error is while updating delivery tariffs", logger.Error(err))
		return nil, err
	}
	return &resp, nil
}

func (d *deliveryTariffRepo) Delete(ctx context.Context, key *pbO.PrimaryKey) (*empty.Empty, error) {
	query := `update delivery_tariffs set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := d.db.Exec(ctx, query, key.GetId())
	if err != nil {
		fmt.Println("error is while deleting delivery tariffs", logger.Error(err))
		return nil, err
	}
	return nil, nil
}

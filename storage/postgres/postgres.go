package postgres

import (
	"context"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/jackc/pgx/v5/pgxpool"
	"order_service/config"
	"order_service/pkg/logger"
	"order_service/storage"
	"strings"

	_ "github.com/golang-migrate/migrate/v4/database"          //database is needed for migration
	_ "github.com/golang-migrate/migrate/v4/database/postgres" //postgres is used for database
	_ "github.com/golang-migrate/migrate/v4/source/file"       //file is needed for migration url
	_ "github.com/lib/pq"
)

type Store struct {
	db  *pgxpool.Pool
	cfg config.Config
	log logger.ILogger
}

func New(ctx context.Context, cfg config.Config, log logger.ILogger) (storage.IStorage, error) {
	url := fmt.Sprintf(
		`postgres://%s:%s@%s:%s/%s?sslmode=disable`,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
	)

	fmt.Println("url", url)
	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil {
		return nil, err
	}

	poolConfig.MaxConns = 100

	pool, err := pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil {
		return nil, err
	}

	//migration
	m, err := migrate.New("file://migrations", url)
	if err != nil {
		return nil, err
	}

	if err = m.Up(); err != nil {
		fmt.Println("herer")
		if !strings.Contains(err.Error(), "no change") {
			fmt.Println("entered", err)
			version, dirty, err := m.Version()
			if err != nil {
				return nil, err
			}

			if dirty {
				fmt.Println("here")
				version--
				if err = m.Force(int(version)); err != nil {
					return nil, err
				}
			}
			fmt.Println("ver", version, "dir", dirty)
			return nil, err
		}
	}

	return Store{
		db:  pool,
		cfg: cfg,
		log: log,
	}, nil
}

func (s Store) Close() {
	s.db.Close()
}

func (s Store) OrderProducts() storage.IOrderProductStorage {
	return NewOrderProductRepo(s.db, s.log)
}

func (s Store) Order() storage.IOrderStorage {
	return NewOrderRepo(s.db, s.log)
}

func (s Store) AlternativeValue() storage.IAlternativeValueStorage {
	return NewAlternativeValueRepo(s.db, s.log)
}

func (s Store) DeliveryTariff() storage.IDeliveryTariffStorage {
	return NewDeliveryTariffRepo(s.db, s.log)
}

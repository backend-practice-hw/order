package storage

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	pbO "order_service/genproto/order_service"
)

type IStorage interface {
	Close()
	OrderProducts() IOrderProductStorage
	Order() IOrderStorage
	AlternativeValue() IAlternativeValueStorage
	DeliveryTariff() IDeliveryTariffStorage
}

type IOrderProductStorage interface {
	Create(context.Context, *pbO.OrderProduct) (*pbO.OrderProduct, error)
	Get(context.Context, *pbO.PrimaryKey) (*pbO.OrderProduct, error)
	GetList(context.Context, *pbO.OrderProductRequest) (*pbO.OrderProductResponse, error)
	Update(context.Context, *pbO.OrderProduct) (*pbO.OrderProduct, error)
	Delete(context.Context, *pbO.PrimaryKey) (*empty.Empty, error)
}

type IOrderStorage interface {
	Create(context.Context, *pbO.Order) (*pbO.Order, error)
	Get(context.Context, *pbO.PrimaryKey) (*pbO.Order, error)
	GetList(context.Context, *pbO.OrderRequest) (*pbO.OrderResponse, error)
	Update(context.Context, *pbO.Order) (*pbO.Order, error)
	Delete(context.Context, *pbO.PrimaryKey) (*empty.Empty, error)
	GetLastBranchOrderID(context.Context, *pbO.Empty) (*pbO.OrderID, error)
}

type IAlternativeValueStorage interface {
	Create(context.Context, *pbO.AlternativeValue) (*pbO.AlternativeValue, error)
	Get(context.Context, *pbO.PrimaryKey) (*pbO.AlternativeValue, error)
	GetList(context.Context, *pbO.GetAlternativeValueRequest) (*pbO.GetAlternativeValueResponse, error)
	Update(context.Context, *pbO.AlternativeValue) (*pbO.AlternativeValue, error)
	Delete(context.Context, *pbO.PrimaryKey) (*empty.Empty, error)
}

type IDeliveryTariffStorage interface {
	Create(context.Context, *pbO.DeliveryTariff) (*pbO.DeliveryTariff, error)
	Get(context.Context, *pbO.PrimaryKey) (*pbO.DeliveryTariff, error)
	GetList(context.Context, *pbO.DeliveryTariffRequest) (*pbO.DeliveryTariffResponse, error)
	Update(context.Context, *pbO.DeliveryTariff) (*pbO.DeliveryTariff, error)
	Delete(context.Context, *pbO.PrimaryKey) (*empty.Empty, error)
}

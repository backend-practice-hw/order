package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	pbO "order_service/genproto/order_service"
	"order_service/grpc/client"
	"order_service/pkg/logger"
	"order_service/service"
	"order_service/storage"
)

func SetUpServer(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *grpc.Server {
	grpcServer := grpc.NewServer()

	pbO.RegisterAlternativeValueServiceServer(grpcServer, service.NewAlternativeValuesService(storage, services, log))
	pbO.RegisterDeliveryTariffServiceServer(grpcServer, service.NewDeliveryTariffService(storage, services, log))
	pbO.RegisterOrderServiceServer(grpcServer, service.NewOrderService(storage, services, log))
	pbO.RegisterOrderProductServiceServer(grpcServer, service.NewOrderProductServiceService(storage, services, log))

	reflection.Register(grpcServer)

	return grpcServer
}

package generate_id

import (
	"fmt"
	"strconv"
)

var number = 1

func GenerateBranchID(name, orderID string) string {
	var id string
	letter := name[:1]

	if orderID == "" {
		numStr := fmt.Sprintf("%06d", number)
		id = letter + "-" + numStr

		return id
	}

	idOrder := orderID[2:]
	num, _ := strconv.Atoi(idOrder)
	num++

	numStr := fmt.Sprintf("%06d", num)

	id = letter + "-" + numStr

	return id
}
